from django.db import models


# Create your models here.

class Hero(models.Model):
    """Implementation of hero table"""
    # api
    name = models.CharField(unique=True, max_length=256)
    # hero
    total_games = models.IntegerField()
    wins = models.IntegerField()
    loses = models.IntegerField()
    winrate = models.FloatField()
    # local
    
    # TODO: image


class HeroStandoff(models.Model):
    """Implementation of hero vs hero winrate table"""
    # first hero name
    first_hero = models.CharField(default=None, max_length=256)
    # second hero name
    second_hero = models.CharField(default=None, max_length=256)
    # total games between first and second hero
    total_games = models.IntegerField()
    # wins versus second hero
    wins = models.IntegerField()
    # loses versus second hero
    loses = models.IntegerField()
    # winrate of hero
    winrate = models.FloatField()

    class Meta:
        unique_together = ('first_hero', 'second_hero',)

    @classmethod
    def get_top_winrates(cls, name):
        return cls.objects.filter(first_hero=name).order_by('-winrate')

    @classmethod
    def get_down_winrates(cls, name):
        return cls.objects.filter(first_hero=name).order_by('winrate')


class Ability(models.Model):
    # one hero to many abilities
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE)
    # name of ability
    name = models.CharField(default=None, max_length=256)
    # title of talent
    title = models.CharField(default=None, max_length=256)
    # short review of ability
    description = models.TextField(default=None)
    # cooldown of ability
    cooldown = models.IntegerField(default=0)
    # manacost of ability
    mana_cost = models.IntegerField(default=0)


class Talent(models.Model):
    # one hero to many talents
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE)
    # name of talent
    name = models.CharField(default=None, unique=True, max_length=256)
    # title of talent
    title = models.CharField(default=None, max_length=256)
    # description of talent
    description = models.TextField(default=None)
    # cooldown
    cooldown = models.IntegerField(default=0)
    # level of talent
    level = models.IntegerField(default=0)
    # pickrate of talent
    rate = models.IntegerField(default=0)

    @classmethod
    def get_tier_1(cls, hero):
        return cls.objects.filter(hero=hero, level=1).order_by('title')

    @classmethod
    def get_tier_4(cls, hero):
        return cls.objects.filter(hero=hero, level=4).order_by('title')

    @classmethod
    def get_tier_7(cls, hero):
        return cls.objects.filter(hero=hero, level=7).order_by('title')

    @classmethod
    def get_tier_10(cls, hero):
        return cls.objects.filter(hero=hero, level=10).order_by('title')

    @classmethod
    def get_tier_13(cls, hero):
        return cls.objects.filter(hero=hero, level=13).order_by('title')

    @classmethod
    def get_tier_16(cls, hero):
        return cls.objects.filter(hero=hero, level=16).order_by('title')
    @classmethod
    def get_tier_20(cls, hero):
        return cls.objects.filter(hero=hero, level=20).order_by('title')
    
    