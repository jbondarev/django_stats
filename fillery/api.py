import requests
import time
import re
import datetime


def get_total_replays_count():
    """
        Get maximal count of replays from hotsapi.net
    """
    # request 
    r = requests.get('https://hotsapi.net/')
    # create regex compiler which removes tags from html file
    p = re.compile(r'<.*?>')
    # remove tags
    tags = p.sub('', r.text).split(' ')
    # numbers list, which should contain only one number
    numbers = []
    # fill numbers list with all numbers from site
    for el in tags:
        string = el.rstrip()
        if string.isdigit():
            numbers.append(int(string))
    # return only one number - replays count
    return numbers[0]


class HotsAPI:
    """
        Implementation of HOTS api using requests lib and hotsapi.net
    """

    def __init__(self):
        self.status_code = 503
        return

    def return_json(self, r):
        self.status_code = r.status_code
        if r.status_code == 200:
            return r.json()
        else:
            return None

    def get_replay_list(self, with_players=None, min_id=None):
        """
           Function, which return 100 replays if with_players = True
           or 1000 if with_players = False
        """
        # settings attrs for api request
        payload = {'with_players': with_players, 'min_id': min_id}
        # request
        r = requests.get("https://hotsapi.net/api/v1/replays/", params=payload)
        # return dictionary of replays
        return self.return_json(r)

    def get_replay(self, id):
        """
           Function, which return replay by id
        """
        # request
        r = requests.get("https://hotsapi.net/api/v1/replays/" + str(id))
        # return dictionary of replay by id
        return self.return_json(r)

    def get_hero_list(self):
        # request
        r = requests.get("https://hotsapi.net/api/v1/heroes/translations")
        # return dictionary of heroes
        return self.return_json(r)

    def get_map_list(self):
        # request
        r = requests.get("https://hotsapi.net/api/v1/maps/translations")
        # return dictionary of maps
        return self.return_json(r)


class HotsManager(object):
    """
        Manage data which has been gotten from requests in api as comfortable list of replays
    """

    def __init__(self):
        # initialization of hots api class
        self.api = HotsAPI()
        # is replays with players or not (default = True)
        self.with_players = True
        # initialization of empty list
        self.replays = []
        # maximal count of replays from hotsapi.net
        self.max_id = self.get_max_id()
        # because of the reason, that hotsapi.net keeps data not in sorted order, just get
        # 10000 replays from maximal id
        # TODO: sort replays by date
        self.min_id = self.max_id - 1000

    def update_last_replays(self):
        """
            Function, which getting 10000 replays from maximal id    
        """
        # Check, if dif between min_id and max_id equals 10000
        if self.max_id - self.min_id != 1000:
            return False
        # clear list before adding something
        self.replays.clear()
        # set iterator
        iterator = self.min_id
        while iterator < self.max_id:
            # we need to get 10000 replays
            try:
                # add gotten replays to manager replays list
                self.replays += self.api.get_replay_list(self.with_players, iterator)
                # increment iterator by 100, if with_players = True, else 1000 (depends on ip) 
                if self.with_players:
                    iterator += 100
                else:
                    iterator += 1000
            except TypeError:
                # This error usually got by 'too-many-request' to api, so we need to wait
                time.sleep(15)
        return True

    def get_max_id(self):
        return get_total_replays_count()