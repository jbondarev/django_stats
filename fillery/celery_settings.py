import os
import django
from celery import Celery
# set environ for celery settings 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
# set celery settings
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'
# create celery app
app = Celery('config')
# set config 
app.config_from_object('django.conf:settings')
# setup django
django.setup()
