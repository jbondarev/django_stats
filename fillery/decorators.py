def exception(function):
    def decorator():
        try:
            return function
        except:
            return 'error'
    return decorator

#20:07