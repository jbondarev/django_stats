from fillery.celery_settings import *
from common.models import (Hero, HeroStandoff, Talent)
from django.db import transaction


@app.task
def hero_manager(replays):
    """Implementation of hero-table-update manager-task"""
    for replay in replays:
        hero_update.delay(replay)
    return 'HERO_MANAGER SUCCESS'


@app.task
def hero_update(replay):
    """Update hero-table"""
    # create 2 lists of winners and losers in replay
    winners, losers = [], []
    # go through players in replay
    for player in replay['players']:
        hero = Hero.objects.get(name=player['hero'])
        talent_choices_update(player)
        hero.total_games = hero.total_games + 1
        if player['winner']:
            winners.append(player['hero'])
            hero.wins = hero.wins + 1
        else:
            losers.append(player['hero'])
            hero.loses = hero.wins + 1
        hero.winrate = round(hero.wins / hero.total_games * 100, 2)
        with transaction.atomic():
            hero.save()
    standoff_manager(winners, losers)
    return 'HERO_UPDATE SUCCESS'


def standoff_manager(winners, losers):
    """Implementation of hero-standoff-table-update manager-task"""
    if (winners is None) or (losers is None) or (len(winners) != 5) or (len(losers) != 5):
        return
    standoff_winners_update.delay(winners, losers)
    standoff_losers_update.delay(winners, losers)


@app.task
def standoff_winners_update(winners, losers):
    """Update winners versus losers rows"""
    for winner in winners:
        for loser in losers:
            hero_winner_name = Hero.objects.get(name=winner).name
            hero_loser_name = Hero.objects.get(name=loser).name
            standoff = HeroStandoff.objects.get(first_hero=hero_winner_name, second_hero=hero_loser_name)
            standoff.total_games = standoff.total_games + 1
            standoff.wins = standoff.wins + 1
            standoff.winrate = round(standoff.wins / standoff.total_games * 100, 2)
            with transaction.atomic():
                standoff.save()
    return 'SUCCESS-1'


@app.task
def standoff_losers_update(winners, losers):
    """Update losers versus winners rows"""
    for winner in winners:
        for loser in losers:
            hero_winner_name = Hero.objects.get(name=winner).name
            hero_loser_name = Hero.objects.get(name=loser).name
            standoff = HeroStandoff.objects.get(first_hero=hero_loser_name, second_hero=hero_winner_name)
            standoff.total_games = standoff.total_games + 1
            standoff.loses = standoff.wins + 1
            standoff.winrate = round(standoff.wins / standoff.total_games * 100, 2)
            with transaction.atomic():
                standoff.save()
    return 'SUCCESS-2'


@app.task
def winrate_changes_manager():
    """Implementation of hero-winrate-changes-table-update manager-task"""
    pass


@app.task
def winrate_changes_update():
    """Update changes"""
    pass


@app.task
def talent_choices_update(player):
    """Update talents"""
    if player['talents'] is not None:
        for level, talent in dict(player['talents']).items()    :
            talent_orm = Talent.objects.get(name=talent)
            talent_orm.rate = talent_orm.rate + 1
            with transaction.atomic():
                talent_orm.save()
        return 'TALENT-CHOICES SUCCESS'
    else:
        return 'TALENT-CHOICES FAILED'