from django.shortcuts import render
from random import randint

# Create your views here.
from django.views.generic import DetailView
from common.models import (Hero, HeroStandoff, Talent, Ability)


class HeroInfoView(DetailView):
    model = Hero
    template_name = 'info/hero.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        name = context['hero'].name
        # TODO: replace with winrate-changes table
        context['random_winrates'] = [randint(45, 75) for _ in range(7)]
        # TODO: replace with popularity-changes table
        context['random_popularity'] = [randint(45, 75) for _ in range(7)]
        # Talents tiers
        context['talents_tier_1'] = Talent.get_tier_1(context['hero'])
        context['talents_tier_4'] = Talent.get_tier_4(context['hero'])
        context['talents_tier_7'] = Talent.get_tier_7(context['hero'])
        context['talents_tier_10'] = Talent.get_tier_10(context['hero'])
        context['talents_tier_13'] = Talent.get_tier_13(context['hero'])
        context['talents_tier_16'] = Talent.get_tier_16(context['hero'])
        context['talents_tier_20'] = Talent.get_tier_20(context['hero'])
        # Abilities
        context['abilities'] = Ability.objects.filter(hero=context['hero']).order_by('title')
        # Top and down winrates of HeroStandoff table

       

        context['top_winrates'] = HeroStandoff.get_top_winrates(name)[0:10]
        context['down_winrates'] = HeroStandoff.get_down_winrates(name)[0:10]
        return context
