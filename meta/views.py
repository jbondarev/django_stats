from django.shortcuts import render
from common.models import Hero
from django.views.generic import TemplateView


# Create your views here.

class MetaView(TemplateView):
    template_name = 'meta/meta.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['heroes'] = Hero.objects.order_by('name')
        return context
