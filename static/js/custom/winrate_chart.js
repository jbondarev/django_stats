let ctx = document.getElementById("myChart").getContext('2d');
let myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['February', 'January', 'December', 'November', 'October', 'September', 'August'],
        datasets: [{
            data: [57, 41, 37, 60, 55, 70, 30],
            fill: false,
            backgroundColor: [
                'rgba(18, 166, 10, 1)',
            ],
            borderColor: [
                'rgba(18, 166, 10, 1)',
            ],
            borderWidth: 5,
            lineTension: 0
        }, {
            data: [50, 50, 50, 50, 50, 50, 50],
            fill: false,
            pointRadius: 0,
            borderColor: [
                'rgba(255,255,255, 1)',
            ],
            borderWidth: 5,
        }]
    },
    options: {
        legend: {
            display: false,
        },
        layout: {
            padding: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    stepSize: 25,
                    min: 25,
                    max: 75,

                },
                gridLines: {
                    color: "#FFFFFF"
                },
            }],
            xAxes: [{
                gridLines: {
                    color: "#FFFFFF"

                }
            }]
        }
    }
});