import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'
django.setup()

from common.models import (Hero, HeroStandoff, Talent, Ability)
from fillery import api
from django.db import transaction

api = api.HotsAPI()


def init_heroes():
    request_data = api.get_hero_list()
    unique_id = 1
    for hero in request_data:
        try:
            Hero.objects.get(name=hero['name'])
        except Hero.DoesNotExist:
            dj_object = Hero(id=unique_id,
                             name=hero['name'],
                             total_games=0,
                             wins=0,
                             loses=0,
                             winrate=0)
            with transaction.atomic():
                dj_object.save()
        unique_id += 1


def get_max_id():
    return len(api.get_hero_list())


def init_heroes_winrate():
    max_id = get_max_id()
    unique_id = 1
    for i in range(max_id):
        for j in range(max_id):
            first_hero = Hero.objects.get(id=i + 1).name
            second_hero = Hero.objects.get(id=j + 1).name
            try:
                HeroStandoff.objects.get(first_hero=first_hero, second_hero=second_hero)
            except HeroStandoff.DoesNotExist:
                dj_object = HeroStandoff(id=unique_id,
                                         first_hero=first_hero,
                                         second_hero=second_hero,
                                         total_games=0,
                                         wins=0,
                                         loses=0,
                                         winrate=0)
                with transaction.atomic():
                    dj_object.save()
            unique_id += 1


def init_talents():
    request_data = api.get_hero_list()
    for hero in request_data:
        for talent in hero['talents']:
            try:
                Talent.objects.get(name=talent['name'])
            except Talent.DoesNotExist:
                if talent['cooldown'] is None:
                    cooldown = 0
                else:
                    cooldown = talent['cooldown']
                dj_object = Talent(hero=Hero.objects.get(name=hero['name']),
                                   name=talent['name'],
                                   title=talent['title'],
                                   level=talent['level'],
                                   description=talent['description'],
                                   cooldown=cooldown,
                                   rate=0)
                with transaction.atomic():
                    dj_object.save()


def init_abilities():
    request_data = api.get_hero_list()
    for hero in request_data:
        for ability in hero['abilities']:
            try:
                Ability.objects.get(name=ability['title'])
            except Ability.DoesNotExist:
                if ability['mana_cost'] is None:
                    mana_cost = 0
                else:
                    mana_cost = ability['mana_cost']
                if ability['cooldown'] is None:
                    cooldown = 0
                else:
                    cooldown = ability['cooldown']
                dj_object = Ability(hero=Hero.objects.get(name=hero['name']),
                                    name=ability['name'],
                                    title=ability['title'],
                                    description=ability['description'],
                                    cooldown=cooldown,
                                    mana_cost=mana_cost)
                with transaction.atomic():
                    dj_object.save()


init_heroes()
init_heroes_winrate()
init_talents()
init_abilities()
